Command line instructions
You can also upload existing files from your computer using the instructions below.


Git global setup
git config --global user.name "Daniel Villarreal"
git config --global user.email "danielvt@gmail.com"

Create a new repository
git clone https://gitlab.com/danielvt/nuxtdevelopmentsamples.git
cd nuxtdevelopmentsamples
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/danielvt/nuxtdevelopmentsamples.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/danielvt/nuxtdevelopmentsamples.git
git push -u origin --all
git push -u origin --tags


# nuxtDevelopmentSamples

> Nuxt development examples to discuss and find help.

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
